require 'test_helper'

class PostsControllerTest < ActionController::TestCase

  setup do
    @post = posts(:one)
  end

  def http_login
    user = 'admin'
    pw = 'secret'
    request.env['HTTP_AUTHORIZATION'] = ActionController::HttpAuthentication::Basic.encode_credentials(user,pw)
  end
  
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:posts)
  end

  test "should get new" do
    http_login
    get :new
    assert_response :success
  end

  test "should create post" do
    assert_difference('Post.count') do
      http_login
      post :create, post: { body: @post.body, title: @post.title }
    end

    assert_redirected_to post_path(assigns(:post))
  end

  test "should show post" do
    get :show, id: @post
    assert_response :success
  end

  test "should get edit" do
    http_login
    get :edit, id: @post
    assert_response :success
  end

  test "should update post" do
    http_login
    patch :update, id: @post, post: { body: @post.body, title: @post.title }
    assert_redirected_to post_path(assigns(:post))
  end

  test "should destroy post" do
    assert_difference('Post.count', -1) do
      http_login
      delete :destroy, id: @post
    end

    assert_redirected_to posts_path
  end
end
